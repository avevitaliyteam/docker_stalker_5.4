#!/bin/sh

STALKER="/var/www/stalker_portal"

if [ ! -d $STALKER/deploy ]; then
        wget -O /tmp/stalker.zip https://bitbucket.org/avevitaliyteam/docker_stalker_5.4/raw/74cbec2607bde8e1732f744129bfd0e4fca61d9f/src/stalker_portal-5.4.0.zip
        unzip /tmp/stalker.zip -d /var/www/
	mv /var/www/stalker_portal-* /var/www/stalker_portal
	#rm -rf /var/www/stalker_portal-*
fi

if [ ! -s $STALKER/server/custom.ini ]; then
	echo '[database]' >> $STALKER/server/custom.ini
	echo 'mysql_host = db' >> $STALKER/server/custom.ini
	echo 'mysql_user = stalker' >> $STALKER/server/custom.ini
	echo 'mysql_pass = stalkerpass' >> $STALKER/server/custom.ini
fi

sed -i '/\bmysql_tzinfo_to_sql\b/g' $STALKER/deploy/build.xml

cd $STALKER/deploy && phing
if [ $? -eq 1 ]; then
	phing
fi

echo Success!

