#!/bin/bash

stalker_port='94'
root_path=$(cd $(dirname $0) && pwd);
image_name=`echo "$root_path" | awk -F "/" '{print $NF}'`
stalker_path="/opt/$image_name"
server_name=172.20.1.87

sed -ri "s/- .*.:88$/- $stalker_port:88/" docker-compose.yml
sed -ri "s/- 33.*.:3306$/- 33$stalker_port:3306/" docker-compose.yml
sed -ri "s~- .*./var/www~- $stalker_path/stalker_www:/var/www~" docker-compose.yml
sed -ri "s~- .*./var/lib/mysql~- $stalker_path/mysql_data:/var/lib/mysql~" docker-compose.yml

docker-compose up -d
docker exec -it ${image_name}_stalker_1 /opt/install_stalker.sh

set_systemd()
{
cat <<-EOF
[Unit]
Description=$image_name service
After=network.target
After=docker.service

[Service]
Restart=always
ExecStart=/usr/bin/docker-compose -f $root_path/docker-compose.yml up
ExecStop=/usr/bin/docker-compose -f $root_path/docker-compose.yml stop

[Install]
WantedBy=multi-user.target
EOF
}
set_systemd > /etc/systemd/system/$image_name.service
systemctl enable $image_name.service

set_nginx_cfg()
{
cat <<-EOF
server {
    listen       80;
    server_name  $server_name;
    access_log  /var/log/nginx/$image_name.access.log;
    error_log   /var/log/nginx/$image_name.error.log;
    location / {
        proxy_pass http://127.0.0.1:$stalker_port/;
        proxy_set_header Host \$host:\$server_port;
        proxy_set_header X-Real-IP \$remote_addr;
    }

    location ~* \.(htm|html|jpeg|jpg|gif|png|css|js)\$ {
        root $stalker_path/stalker_www;
        expires 30d;
    }
}

EOF
}

set_nginx_cfg > /etc/nginx/sites-available/$image_name
echo "Please edit servername /etc/nginx/sites-available/$image_name and enable it"